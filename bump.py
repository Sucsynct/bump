import bpy, json

with open("PATH_TO_ROTATION_DATA") as f:
    rot_paths = json.load(f)
    
with open("PATH_TO_LOCATION_DATA") as f:
    loc_paths = json.load(f)

skeleton = bpy.data.objects["Armature"]

bpy.data.objects['Armature'].animation_data_clear()

bones = {}
for key in loc_paths.keys():
    bones[key] = skeleton.pose.bones[key]
    
for bone in bones.keys():
    if bone in rot_paths.keys():
        for frm, euler_rot in enumerate(rot_paths[bone]):
            bones[bone].rotation_euler = euler_rot
            bones[bone].keyframe_insert(data_path="rotation_euler", frame=frm)
        
for frm, loc in enumerate(loc_paths['Hip']):
    bones['Hip'].location = loc
    bones['Hip'].keyframe_insert(data_path="location", frame=frm)

bpy.data.scenes['Scene'].frame_end = len(loc_paths['Hip'])
