# Blender User-defined Motion Path (BuMP) Addon

This addon's purpose is to facilitate importing of an external motion paths or other keyframe data into Blender for the purpose of animating armatures.
